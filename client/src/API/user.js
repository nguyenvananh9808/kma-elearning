import axios from 'axios';

const apiUser = axios.create({
    baseURL: 'http://localhost:8000/api/QuanLyNguoiDung/'
})

export default apiUser;