import { Field, Form, Formik } from 'formik';
import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import apiUser from '../../API/user';
import Swal from "sweetalert2";
import { uploadAvatar, logout } from '../../Redux/Actions/user';

const Profile = (props) => {
    const [avatar, setAvatar] = useState(props.user.avatar);

    const userLocalStorage = localStorage.getItem("user");
    const userLocalStorageParse = JSON.parse(userLocalStorage);

    useEffect(() => {
        if (userLocalStorage)
            setAvatar(userLocalStorageParse.avatar)
    }, []) 

    const getDayMonthYear = (time) => {
        const d = new Date(time);
        const day = d.getDate();
        const month = d.getMonth() + 1;
        const year = d.getFullYear();

        return [day, month, year].join('/');
    }

    const handleChangeImage = (event) => {
        setAvatar(URL.createObjectURL(event.target.files[0]));
        const formData = new FormData();
        formData.append("avatar", event.target.files[0]);
        props.uploadAvatar(props.user._id, formData)
    }

    return (
        <div className="profile_container">
            <div className="card_wrap container">
                <div className="card container-fluid">
                    <div className="card-body">
                        <Formik
                            initialValues={{
                                password: '',
                            }}
                            onSubmit={values => {
                                apiUser
                                    .put(`Doipassword?_id=${props.user._id}`, values)
                                    .then((result) => {
                                        Swal.fire({
                                            icon: "success",
                                            title: "Bạn đã đổi mật khẩu thành công",
                                            confirmButtonColor: '#e74c3c',
                                            confirmButtonText: 'Ok'
                                        }).then(() => props.logout())
                                            .then(() => props.history.push('/'))
                                    })
                                    .catch((err) => {
                                        if (err.response.status === 400) {
                                            Swal.fire({
                                                title: 'Bạn đã đổi mật khẩu thất bại',
                                                icon: 'error',
                                                confirmButtonColor: '#e74c3c',
                                                confirmButtonText: 'Ok'
                                            })
                                        }
                                    })
                            }}
                            render={(formikProps) => {
                                return <Form >
                                    <div className="profile_avatar">
                                        <label className="profile_avatar_label" style={{ backgroundImage: `url(${avatar})`, width: "90px", height: "90px" }}>
                                            <input name="avatar" id="avatar" type="file" className="profile_avatar_input" onChange={(event) => handleChangeImage(event)} />
                                        </label>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="fullname">Họ tên: </label>
                                        <Field id="fullname" type="text" className="form-control" name="fullname" readOnly value={props.user.fullname} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="username">Tài khoản: </label>
                                        <Field id="username" type="text" className="form-control" name="username" readOnly value={props.user.username} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="phoneNumber">Số điện thoại: </label>
                                        <Field id="phoneNumber" type="text" className="form-control" name="soDT" readOnly value={props.user.phoneNumber} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="email">Email: </label>
                                        <Field id="email" type="email" className="form-control" name="email" readOnly value={props.user.email} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="role">Mã loại người dùng: </label>
                                        <Field id="role" type="text" className="form-control" name="role" readOnly value={props.user.role} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="createAt">Ngày tham gia: </label>
                                        <Field id="createAt" type="text" className="form-control" name="createAt" readOnly value={getDayMonthYear(props.user.createAt)} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="password">Mật khẩu: </label>
                                        <Field id="password" type="password" className="form-control password" name="password" placeholder="Mật khẩu mới" onChange={formikProps.handleChange} />
                                    </div>
                                    <div className="text-center">
                                        <button
                                            type="submit"
                                            className="btn btn-primary m-2"
                                        >
                                            LƯU
                                            </button>
                                    </div>
                                </Form>
                            }} />
                    </div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.userReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        uploadAvatar: (_id, formData) => {
            dispatch(uploadAvatar(_id, formData))
        },
        logout: () => {
            dispatch(logout());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);