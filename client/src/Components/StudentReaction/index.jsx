import React from 'react';
import Slider from "react-slick";

const StudentReaction = () => {
    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
    };

    return (
        <div className="student_reaction">
            <div className="student_reaction_container container">
                <h3 className="text-center">Cảm nghĩ của học viên</h3>
                <Slider {...settings}>
                    <div className="student_reaction_item">
                        <div className="student_reaction_person">
                            <div className="student_reaction_avatar ava1">
                            </div>
                            <p className="student_reaction_name">Lê Đình Thọ</p>
                        </div>
                        <p className="student_reaction_comment">
                            “Nền tảng học trực tuyến đã giúp tôi học code hiệu quả hơn. Bên cạnh các khóa học và bài tập phù hợp.“
                        </p>
                    </div>

                    <div className="student_reaction_item">
                        <div className="student_reaction_person">
                            <div className="student_reaction_avatar ava2">
                            </div>
                            <p className="student_reaction_name">Nông Thành Hợp</p>
                        </div>
                        <p className="student_reaction_comment">
                            “Nền tảng học trực tuyến đã giúp tôi học code hiệu quả hơn. Bên cạnh các khóa học và bài tập phù hợp.“
                        </p>
                    </div>

                    <div className="student_reaction_item">
                        <div className="student_reaction_person">
                            <div className="student_reaction_avatar ava3">
                            </div>
                            <p className="student_reaction_name">Nguyễn Đức Chính</p>
                        </div>
                        <p className="student_reaction_comment">
                            “Nền tảng học trực tuyến đã giúp tôi học code hiệu quả hơn. Bên cạnh các khóa học và bài tập phù hợp.“
                        </p>
                    </div>

                    <div className="student_reaction_item">
                        <div className="student_reaction_person">
                            <div className="student_reaction_avatar ava4">
                            </div>
                            <p className="student_reaction_name">Hoàng Trung Nguyên</p>
                        </div>
                        <p className="student_reaction_comment">
                            “Nền tảng học trực tuyến đã giúp tôi học code hiệu quả hơn. Bên cạnh các khóa học và bài tập phù hợp.“
                        </p>
                    </div>

                    <div className="student_reaction_item">
                        <div className="student_reaction_person">
                            <div className="student_reaction_avatar ava5">
                            </div>
                            <p className="student_reaction_name">Nguyễn Tùng Anh</p>
                        </div>
                        <p className="student_reaction_comment">
                            “Nền tảng học trực tuyến đã giúp tôi học code hiệu quả hơn. Bên cạnh các khóa học và bài tập phù hợp.“
                        </p>
                    </div>

                    <div className="student_reaction_item">
                        <div className="student_reaction_person">
                            <div className="student_reaction_avatar ava6">
                            </div>
                            <p className="student_reaction_name">Nguyễn Thị Linh</p>
                        </div>
                        <p className="student_reaction_comment">
                            “Nền tảng học trực tuyến đã giúp tôi học code hiệu quả hơn. Bên cạnh các khóa học và bài tập phù hợp.“
                        </p>
                    </div>
                </Slider>
            </div>
        </div>
    );
};

export default StudentReaction;