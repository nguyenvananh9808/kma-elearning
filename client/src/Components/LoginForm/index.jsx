import React, { useState } from 'react';
import { connect } from "react-redux";
import { toggleLogin } from '../../Redux/Actions/dialog';
import { login } from '../../Redux/Actions/user';
import apiUser from '../../API/user';
import Swal from "sweetalert2";
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Button } from "@material-ui/core";

const LoginForm = (props) => {
    const [fullWidth, setFullWidth] = useState(true);
    const [maxWidth, setMaxWidth] = useState('sm');

    const loginSchema = yup.object().shape({
        username: yup.string().required('* Không được bỏ trống!'),
        password: yup.string().required('* Không được bỏ trống!'),
    })

    return (
        <React.Fragment>
            <button className="btn mr-1 btn--white" type="submit" onClick={props.toggleLogin}>Đăng nhập</button>
            <Dialog open={props.toggle.toggleLogin} onClose={props.toggle.toggleLogin} aria-labelledby="form-dialog-title" fullWidth={fullWidth} maxWidth={maxWidth}>
                <DialogTitle id="form-dialog-title">ĐĂNG NHẬP</DialogTitle>
                <DialogContent>
                    <Formik
                        initialValues={{
                            username: '',
                            password: '',
                        }}
                        validationSchema={loginSchema}
                        onSubmit={values => {
                            apiUser
                                .post("DangNhap", values)
                                .then(result => {
                                    props.login(result.data.user);
                                })
                                .then(() => {
                                    Swal.fire({
                                        title: 'Bạn đã đăng nhập thành công',
                                        icon: 'success',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Ok'
                                    })
                                })
                                .catch((err) => {
                                    if (err.response.status === 400) {
                                        Swal.fire({
                                            title: 'Tài khoản hoặc mật khẩu không đúng!',
                                            icon: 'error',
                                            confirmButtonColor: '#e74c3c',
                                            confirmButtonText: 'Ok'
                                        }).then(() => props.toggleLogin())
                                    }
                                })
                        }}
                        render={(formikProps) => {
                            return <Form>
                                <div className="form-group">
                                    <label htmlFor="username">Tài khoản: </label>
                                    <Field id="username" type="text" className="form-control" name="username" onChange={formikProps.handleChange} />
                                    <ErrorMessage name="username">
                                        {(msg) => <div className="alert alert-danger">{msg}</div>}
                                    </ErrorMessage>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password">Mật khẩu: </label>
                                    <Field id="password" type="password" className="form-control" name="password" onChange={formikProps.handleChange} />
                                    <ErrorMessage name="password">
                                        {(msg) => <div className="alert alert-danger">{msg}</div>}
                                    </ErrorMessage>
                                </div>
                                <div className="text-center">
                                    <button type="submit" onClick={props.toggleLogin} disabled={!(formikProps.isValid && formikProps.dirty)} className="btn btn-danger m-2">ĐĂNG NHẬP</button>
                                    <Button onClick={props.toggleLogin}>Huỷ</Button>
                                </div>
                            </Form>
                        }} />
                </DialogContent>
            </Dialog>
        </React.Fragment>
    );
};

const mapStateToProps = (state) => {
    return {
        toggle: state.dialogReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleLogin: () => {
            dispatch(toggleLogin())
        },
        login: (user) => {
            dispatch(login(user))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);