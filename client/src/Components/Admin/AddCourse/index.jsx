import React, { useState, useEffect } from 'react';
import Swal from "sweetalert2";
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';
import apiCourse from '../../../API/courses';
import { connect } from 'react-redux';
import { getCourseDetail } from '../../../Redux/Actions/courses';
import Button from '@material-ui/core/Button';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import InputAddLesson from './InputAddLesson';

const getSteps = () => {
    return ['Thêm khoá học', 'Thêm bài học'];
}

const AddCourse = (props) => {
    const [activeStep, setActiveStep] = React.useState(0);
    const [lessonInput, setLessonInput] = React.useState([{ nameLesson: '', linkVideo: '' }]);
    const steps = getSteps();

    useEffect(() => {
        props.getCourseDetail(JSON.parse(localStorage.getItem('idCourse')))
    }, [])

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }

    const userLocalStorage = localStorage.getItem("user");
    const userLocalStorageParse = JSON.parse(userLocalStorage);

    const AddCourseSchema = yup.object().shape({
        name: yup.string().required('* Không được bỏ trống!'),
        description: yup.string().required('* Không được bỏ trống!'),
        thumbnail: yup.string().required('* Không được bỏ trống!'),
        idTeacher: yup.string(),
        tag: yup.string().ensure().required('* Không được bỏ trống!'),
    })

    const addLessonInput = () => {
        setLessonInput([...lessonInput, { nameLesson: '', linkVideo: '' }])
    }

    const doneAddCourse = () => {
        apiCourse
            .post(`ThemBaiHocVaoidModule?_id=${props.courseDetail._id}`)
            .then(() => {
                Swal.fire({
                    title: 'Đã hoàn thành cập nhật mục lục!',
                    icon: 'success',
                    confirmButtonColor: '#e74c3c',
                    confirmButtonText: 'Ok',
                }).then(() => { props.history.push('/admin/courselist') })
            })
    }

    const removeLessonInput = (i) => {
        let lessonInputCopy = [...lessonInput];
        lessonInputCopy.splice(i, 1);
        setLessonInput(lessonInputCopy);
    }

    return (
        <div className="addCourse_wrapper container">
            <Stepper className="mb-3" activeStep={activeStep} alternativeLabel>
                {steps.map((label) => (
                    <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                    </Step>
                ))}
            </Stepper>
            {activeStep === 0 ? (
                <Formik
                    initialValues={{
                        name: '',
                        description: '',
                        thumbnail: '',
                        idTeacher: userLocalStorageParse.username,
                        tag: '',
                    }}
                    validationSchema={AddCourseSchema}
                    onSubmit={values => {
                        apiCourse
                            .post("ThemKhoaHoc", values)
                            .then((result) => {
                                Swal.fire({
                                    icon: "success",
                                    title: "Thêm khoá học thành công",
                                    confirmButtonColor: '#e74c3c',
                                    confirmButtonText: 'Ok'
                                }).then(() => localStorage.setItem('idCourse', JSON.stringify(result.data.data._id)))
                                    .then(() => props.getCourseDetail(JSON.parse(localStorage.getItem('idCourse'))))
                                    .then(() => handleNext())
                            })
                            .catch((err) => {
                                Swal.fire({
                                    title: 'Thêm khoá học thất bại',
                                    icon: 'error',
                                    confirmButtonColor: '#e74c3c',
                                    confirmButtonText: 'Ok'
                                })
                            })
                    }}
                    render={(formikProps) => {
                        return <Form >
                            <div className="form-group">
                                <label htmlFor="name">Tên khoá học: </label>
                                <Field id="name" type="text" className="form-control" name="name" onChange={formikProps.handleChange} />
                                <ErrorMessage name="name">
                                    {(msg) => <div className="alert alert-danger">{msg}</div>}
                                </ErrorMessage>
                            </div>
                            <div className="form-group">
                                <label htmlFor="description">Mô tả: </label>
                                <Field id="description" type="text" className="form-control" name="description" onChange={formikProps.handleChange} />
                                <ErrorMessage name="description">
                                    {(msg) => <div className="alert alert-danger">{msg}</div>}
                                </ErrorMessage>
                            </div>
                            <div className="form-group">
                                <label htmlFor="thumbnail">Hình ảnh: </label>
                                <Field id="thumbnail" type="text" className="form-control" name="thumbnail" onChange={formikProps.handleChange} />
                                <ErrorMessage name="thumbnail">
                                    {(msg) => <div className="alert alert-danger">{msg}</div>}
                                </ErrorMessage>
                            </div>
                            <div className="form-group">
                                <label htmlFor="idTeacher">Tài khoản người tạo: </label>
                                <Field id="idTeacher" type="text" className="form-control" name="idTeacher" readOnly value={userLocalStorageParse.username} />
                                <ErrorMessage name="idTeacher">
                                    {(msg) => <div className="alert alert-danger">{msg}</div>}
                                </ErrorMessage>
                            </div>
                            <div className="form-group">
                                <label htmlFor="tag">Mã danh mục: </label>
                                <Field as="select" name="tag" className="form-control">
                                    <option value="">Chọn mã danh mục...</option>
                                    <option value="Frontend">Frontend</option>
                                    <option value="Backend">Backend</option>
                                    <option value="Mobile">Mobile</option>
                                    <option value="Design">Design</option>
                                </Field>
                                <ErrorMessage name="tag">
                                    {(msg) => <div className="alert alert-danger">{msg}</div>}
                                </ErrorMessage>
                            </div>
                            <div className="text-center">
                                <button type="submit" disabled={!(formikProps.isValid && formikProps.dirty)} className="btn btn-danger m-2">THÊM KHOÁ HỌC</button>
                                <Button onClick={formikProps.handleReset}>ĐẶT LẠI</Button>
                            </div>
                        </Form>
                    }} />
            ) : (
                    <div className="addLesson_wrapper">
                        <button className="btn btn-success text-center btnAddLesson mb-4" onClick={addLessonInput}>THÊM BÀI HỌC</button>
                        {lessonInput.map((item, index) => {
                            return (
                                <div key={index} index={index} className="addLesson_content_wrapper">
                                    <InputAddLesson id={index} idCourse={props.courseDetail && props.courseDetail._id} item={item} />
                                    <div className="addLesson_btnCancel">
                                        <button className="btn btn-danger btnRemoveLessonInput" onClick={() => removeLessonInput(index)}>HUỶ</button>
                                    </div>
                                </div>
                            )
                        })}
                        <button className="btn btn-danger text-center btnComplete mt-4" onClick={doneAddCourse}>HOÀN THÀNH</button>
                    </div>
                )}

        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        courseDetail: state.courseDetailReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCourseDetail: (_id) => {
            dispatch(getCourseDetail(_id));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddCourse);