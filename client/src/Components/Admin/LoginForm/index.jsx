import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';
import Swal from "sweetalert2";
import apiUser from '../../../API/user';
import { connect } from 'react-redux';
import { login } from '../../../Redux/Actions/user';

const AdminLoginForm = (props) => {
    const loginSchema = yup.object().shape({
        username: yup.string().required('* Không được bỏ trống!'),
        password: yup.string().required('* Không được bỏ trống!'),
    })

    return (
        <div className="admin_login_form_wrapper container">
            <Formik
                className="admin_login_form"
                initialValues={{
                    username: '',
                    password: '',
                }}
                validationSchema={loginSchema}
                onSubmit={values => {
                    apiUser
                        .post("DangNhap", values)
                        .then(result => {
                            props.login(result.data.user);
                        })
                        .then(() => {
                            Swal.fire({
                                title: 'Bạn đã đăng nhập thành công',
                                icon: 'success',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Ok'
                            }).then(() => {
                                if (props.user.role === 'HV') {
                                    props.history.push('/');
                                }
                                else if (props.user.role !== 'HV') {
                                    props.history.push('/admin');
                                }
                            })

                        })
                        .catch((err) => {
                            if (err.response.status === 400) {
                                Swal.fire({
                                    title: 'Tài khoản hoặc mật khẩu không đúng!',
                                    icon: 'error',
                                    confirmButtonColor: '#e74c3c',
                                    confirmButtonText: 'Ok'
                                })
                            }
                        })
                }}
                render={(formikProps) => {
                    return <Form>
                        <div className="form-group">
                            <label htmlFor="username">Tài khoản: </label>
                            <Field id="username" type="text" className="form-control" name="username" onChange={formikProps.handleChange} />
                            <ErrorMessage name="username">
                                {(msg) => <div className="alert alert-danger">{msg}</div>}
                            </ErrorMessage>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Mật khẩu: </label>
                            <Field id="password" type="password" className="form-control" name="password" onChange={formikProps.handleChange} />
                            <ErrorMessage name="password">
                                {(msg) => <div className="alert alert-danger">{msg}</div>}
                            </ErrorMessage>
                        </div>
                        <div className="text-center">
                            <button type="submit" disabled={!(formikProps.isValid && formikProps.dirty)} className="btn btn-danger m-2">ĐĂNG NHẬP</button>
                        </div>
                    </Form>
                }} />
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.userReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        login: (user) => {
            dispatch(login(user))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminLoginForm);