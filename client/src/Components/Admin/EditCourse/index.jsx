import React, { useEffect } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';
import Swal from "sweetalert2";
import apiCourse from '../../../API/courses';
import { connect } from 'react-redux';
import { getCourseDetail } from '../../../Redux/Actions/courses';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const EditCourse = (props) => {
    useEffect(() => {
        props.getCourseDetail(props.match.params._id);
    }, [])

    const courseSchema = yup.object().shape({
        name: yup.string().required('* Không được bỏ trống!'),
        description: yup.string().required('* Không được bỏ trống!'),
        thumbnail: yup.string().required('* Không được bỏ trống!'),
        idTeacher: yup.string().required('* Không được bỏ trống!'),
        tag: yup.string(),
    })

    return (
        <div className="editCourse_wrapper container">
            <Typography className="text-center" variant="h4" id="tableTitle" component="div">
                Sửa khoá học
            </Typography>
            <Formik
                enableReinitialize
                initialValues={{
                    name: props.courseDetail && props.courseDetail.name,
                    description: props.courseDetail && props.courseDetail.description,
                    thumbnail: props.courseDetail && props.courseDetail.thumbnail,
                    idTeacher: props.courseDetail && props.courseDetail.idTeacher,
                    tag: props.courseDetail && props.courseDetail.tag,
                }}
                validationSchema={courseSchema}
                onSubmit={values => {
                    apiCourse
                        .put(`SuaKhoaHoc?_id=${props.courseDetail._id}`, values)
                        .then(() => {
                            Swal.fire({
                                title: 'Đã sửa khoá học thành công!',
                                icon: 'success',
                                confirmButtonColor: '#e74c3c',
                                confirmButtonText: 'Ok',
                            }).then(() => props.history.push('/admin/courselist'))
                        })
                }}
                render={(formikProps) => {
                    return <Form>
                        <div className="form-group">
                            <label htmlFor="id">ID: </label>
                            <Field id="id" type="text" className="form-control" name="id" readOnly value={props.courseDetail._id} />
                            <ErrorMessage name="id">
                                {(msg) => <div className="alert alert-danger">{msg}</div>}
                            </ErrorMessage>
                        </div>
                        <div className="form-group">
                            <label htmlFor="name">Tên khoá học: </label>
                            <Field id="name" type="text" className="form-control" name="name" onChange={formikProps.handleChange} />
                            <ErrorMessage name="name">
                                {(msg) => <div className="alert alert-danger">{msg}</div>}
                            </ErrorMessage>
                        </div>
                        <div className="form-group">
                            <label htmlFor="description">Mô tả: </label>
                            <Field id="description" type="text" className="form-control" name="description" onChange={formikProps.handleChange} />
                            <ErrorMessage name="description">
                                {(msg) => <div className="alert alert-danger">{msg}</div>}
                            </ErrorMessage>
                        </div>
                        <div className="form-group">
                            <label htmlFor="thumbnail">Hình ảnh: </label>
                            <Field id="thumbnail" type="text" className="form-control" name="thumbnail" onChange={formikProps.handleChange} />
                            <ErrorMessage name="thumbnail">
                                {(msg) => <div className="alert alert-danger">{msg}</div>}
                            </ErrorMessage>
                        </div>
                        <div className="form-group">
                            <label htmlFor="idTeacher">Tài khoản người tạo: </label>
                            <Field id="idTeacher" type="text" className="form-control" name="idTeacher" readOnly onChange={formikProps.handleChange} />
                            <ErrorMessage name="idTeacher">
                                {(msg) => <div className="alert alert-danger">{msg}</div>}
                            </ErrorMessage>
                        </div>
                        <div className="form-group">
                            <label htmlFor="tag">Mã danh mục: </label>
                            <Field as="select" name="tag" className="form-control" placeholder={props.courseDetail && props.courseDetail.tag} onChange={formikProps.handleChange}>
                                <option value="Frontend">Frontend</option>
                                <option value="Backend">Backend</option>
                                <option value="Mobile">Mobile</option>
                                <option value="Design">Design</option>
                            </Field>
                            <ErrorMessage name="tag">
                                {(msg) => <div className="alert alert-danger">{msg}</div>}
                            </ErrorMessage>
                        </div>
                        <div className="text-center">
                            <Button variant="contained" color="primary" type="submit" disabled={!(formikProps.isValid && formikProps.dirty)}>HOÀN THÀNH</Button>
                        </div>
                    </Form>
                }} />
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        courseDetail: state.courseDetailReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCourseDetail: (_id) => {
            dispatch(getCourseDetail(_id))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditCourse);