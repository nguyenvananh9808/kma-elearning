const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    fullname: String,
    username: { type: String, unique: true },
    password: String,
    phoneNumber: String,
    email: String,
    avatar: { type: String, default: 'https://res.cloudinary.com/avata-user-store/image/upload/v1587982981/n6rdmmtxyjr0gttp8zo7.png' },
    createAt: { type: Date, default: Date.now },
    role: { type: String, default: 'HV' },
});

const User = mongoose.model('User', UserSchema);
module.exports = User;