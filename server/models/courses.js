const mongoose = require('mongoose');

const CourseSchema = new mongoose.Schema({
    name: String,
    description: String,
    thumbnail: String,
    idTeacher: String,
    tag: String,
    idModule: Array,
})

const Course = mongoose.model('Course', CourseSchema);

module.exports = Course;