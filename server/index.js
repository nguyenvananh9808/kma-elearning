const mongoose = require('mongoose');
const Course = require('./models/courses');
const User = require('./models/user');
const Lesson = require('./models/lessons');
const express = require('express');
const cors = require('cors')
require('dotenv').config();
const { json, urlencoded } = require("body-parser");
const { hash, compare } = require('bcryptjs');
const app = express();
const { verifyPromise, signPromise } = require('./utils/jwt');
const cloudinary = require('cloudinary')
const formData = require('express-form-data')

app.use(json());
app.use(cors());
app.use(urlencoded({ extended: true }));
app.use(formData.parse())

cloudinary.config({
    cloud_name: "avata-user-store",
    api_key: "133444667541392",
    api_secret: "iC2MfZ2PUSGm8MoVcYFK1xq0dMI"
})

// const conn = mongoose.createConnection('mongodb+srv://abc:12345678910@course-eon1f.mongodb.net/test?retryWrites=true&w=majority')

mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log("Connected to MongoDB"))
    .catch((err) => console.log("Error" + err))

// Course api
app.get('/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc', (req, res) => {
    Course.find({})
        .then(courses => res.send({ success: true, courses }))
        .catch(err => res.set({ success: false, err }))
});

app.post("/api/QuanLyKhoaHoc/ThemKhoaHoc", (req, res) => {
    const { name, description, thumbnail, idTeacher, tag, idModule } = req.body
    if (name.trim() === '' || description.trim() === '' || thumbnail.trim() === '' || idTeacher.trim() === '' || tag.trim() === '') {
        res.status(401);
        return res.send({ success: false, message: "Emty value" });
    }
    const newCourse = new Course({ name, description, thumbnail, idTeacher, tag, idModule })
    newCourse.save()
        .then(course => res.send({ success: true, data: course }))
        .catch(error => res.send({ success: false, message: error }))
})

app.delete("/api/QuanLyKhoaHoc/XoaKhoaHoc", async (req, res) => {
    const _id = req.query._id;
    Course.findOneAndDelete({ _id })
        .then(course => res.send({ success: true, data: course }))
        .catch(error => res.send({ success: false, message: error }))
})

app.put('/api/QuanLyKhoaHoc/SuaKhoaHoc', (req, res) => {
    const _id = req.query._id;
    const { name, description, thumbnail, idTeacher, tag } = req.body;

    Course.findOneAndUpdate({ _id }, { name, description, thumbnail, idTeacher, tag }, { new: true })
        .then(course => {
            if (!course) throw new Error("EMPTY_COURSE");
            res.send({ success: true, course: course });
        })
        .catch(error => res.send({ success: false, message: error.message }));

});

app.get('/api/QuanLyKhoaHoc/LayKhoaHocTheoDanhMuc', async (req, res) => {
    try {
        const tag = req.query.tag;
        const courses = await Course.find({ tag })
        res.status(200).send({ success: true, courses })

    } catch (error) {
        res.status(400).send({ success: false, error })
    }
})

app.get('/api/QuanLyKhoaHoc/LayThongTinKhoaHoc', async (req, res) => {
    try {
        const _id = req.query._id;
        const courseDetail = await Course.findOne({ _id })
        res.status(200).send({ success: true, courseDetail })
    } catch (error) {
        res.status(401).send({ success: false, error })
    }
})

app.get('/api/QuanLyKhoaHoc/LayDanhSachBaiHoc', (req, res) => {
    Lesson.find({})
        .then(lessons => res.send({ success: true, lessons }))
        .catch(err => res.set({ success: false, err }))
})

app.get('/api/QuanLyKhoaHoc/LayThongTinBaiHoc', async (req, res) => {
    try {
        const _id = req.query._id;
        const lessonDetail = await Lesson.findOne({ _id })
        res.status(200).send({ success: true, lessonDetail })
    } catch (error) {
        res.status(401).send({ success: false, error })
    }
})

app.post('/api/QuanLyKhoaHoc/ThemBaiHoc', async (req, res) => {
    try {
        const _id = req.query._id;
        const { nameLesson, linkVideo } = req.body;

        const newLesson = new Lesson({
            idCourse: _id,
            nameLesson: nameLesson,
            linkVideo: linkVideo
        })
        newLesson.save()
        res.status(200).send({ success: true, newLesson })
    }
    catch (err) {
        res.status(400).send({ success: false, message: err.message })
    }

})

app.post('/api/QuanLyKhoaHoc/ThemBaiHocVaoidModule', async (req, res) => {
    const _id = req.query._id;

    const arrayidModule = []

    const data = await Lesson.find({})
    data.map(lesson => {
        if (lesson.idCourse === _id) {
            arrayidModule.push(lesson)
        }
    })

    if (arrayidModule.length > 0) {
        Course.findOneAndUpdate({ _id }, { idModule: arrayidModule }, { new: true })
            .then(course => {
                if (!course) throw new Error("EMPTY_COURSE");
                res.status(200).send({ success: true, course: course });
            })
            .catch(error => res.status(400).send({ success: false, message: error.message }));
    }
});


//USER api

app.get('/api/QuanLyNguoiDung/LayDanhSachNguoiDung', async (req, res) => {
    try {
        const users = await User.find({})
        res.status(200).send({ success: true, users })
    } catch (error) {
        res.status(error.status).set({ success: false, error })
    }
})

app.get('/api/QuanLyNguoiDung/LayDanhSachHocVien', async (req, res) => {
    try {
        const students = await User.find({ role: 'HV' })
        res.status(200).send({ success: true, students })
    } catch (error) {
        res.status(400).send({ success: false, message: error.message })
    }
})

app.get('/api/QuanLyNguoiDung/LayNguoiDungTheoID', async (req, res) => {
    try {
        const _id = req.query._id;
        const userDetail = await User.findOne({ _id })
        res.status(200).send({ success: true, userDetail })
    } catch (error) {
        res.status(401).send({ success: false, error })
    }
})

app.post('/api/QuanLyNguoiDung/DangKy', async (req, res) => {
    try {
        const { fullname, username, password, phoneNumber, email, role } = req.body;
        const encrypt = await hash(password, 8);
        const user = new User({ fullname, username, password: encrypt, phoneNumber, email, role });
        if (await User.findOne({ username })) {
            res.status(401).send({ success: false, message: "Tài khoản đã tồn tại!" });
            throw 'Tài khoản "' + username + '" đã tồn tại!';
        }
        else {
            await user.save();
            res.status(200).send({ success: true, user })
        }
    } catch (error) {
        res.status(401).send({ success: false, error })
    }
})

app.post('/api/QuanLyNguoiDung/DangNhap', async (req, res) => {
    try {
        const { username, password } = req.body;
        const user = await User.findOne({ username });
        if (!user) {
            return res.status(400).send({ success: false, message: 'INVALID_USER_INFO' })
        }

        const passwordValidated = await compare(password, user.password);
        if (!passwordValidated) {
            return res.status(400).send({ success: false, message: 'INVALID_USER_INFO' })
        }

        const token = await signPromise({ username: user.username });
        const userInfo = user.toObject();
        userInfo.password = undefined;
        userInfo.token = token;
        res.send({ success: true, user: userInfo });
    } catch (error) {
        res.status(400).send({ success: false, error })
    }
})

app.put('/api/QuanLyNguoiDung/UploadAvatar', async (req, res) => {
    try {
        const _id = req.query._id;
        const path = req.files.avatar.path;

        cloudinary.uploader.upload(path)
            .then(image => {
                User.findOneAndUpdate({ _id }, { avatar: image.url }, { new: true })
                    .then(newInfoUser => {
                        const user = newInfoUser;
                        user.password = undefined;
                        res.status(200).send({ success: true, user })
                    })
            })
    }
    catch (error) {
        res.status(400).send({ success: false, error })
    }
})

app.put('/api/QuanLyNguoiDung/Doipassword', async (req, res) => {
    try {
        const _id = req.query._id;
        const { password } = req.body;
        const encrypt = await hash(password, 8);
        User.findOneAndUpdate({ _id }, { password: encrypt }, { new: true })
            .then(newInfoUser => {
                const user = newInfoUser;
                user.password = undefined;
                res.status(200).send({ success: true, user });
            })
    }
    catch (err) {
        res.status(400).send({ success: false, message: err })
    }
})

app.delete("/api/QuanLyNguoiDung/XoaHocVien", async (req, res) => {
    const _id = req.query._id;
    User.findOneAndDelete({ _id })
        .then(student => res.send({ success: true, data: student }))
        .catch(error => res.send({ success: false, message: error }))
})

app.listen(process.env.PORT || 3005, () => console.log('Server running in port: ' + process.env.PORT))


